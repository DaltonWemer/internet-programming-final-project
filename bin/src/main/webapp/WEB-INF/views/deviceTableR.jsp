<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<%@ include file="/WEB-INF/layouts/head.jsp"%>
<body id="demo-body">
	<div id="demo-main-div" class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<h1>Device Inventory</h1>

				<c:if test="${empty deviceList}">
					There are no items in the inventory database. 
				</c:if>

				<table class="table table-bordered table-hover">
					<tr>
						<th>ID</th>
						<th>Barcode</th>
						<th>Name</th>
						<th>Category</th>
						<th>Condition</th>
						<th>Price</th>
					</tr>

					<c:if test="${not empty deviceList}">
						<c:forEach var="device" items="${deviceList}">
							<tr>
								<td>${device.id}</td>
								<td>${device.barcode}</td>
								<td>${device.name}</td>
								<td>${device.category}</td>
								<td>${device.condition}</td>
								<td>${device.price}</td>
							</tr>
						</c:forEach>
					</c:if>
				</table>
			</div>
		</div>
	</div>
</body>
</html>