package edu.missouristate.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.missouristate.model.Device;
import edu.missouristate.services.DeviceService;


@Controller
public class HomeController {
	
	@Autowired
	DeviceService deviceService;
	
	/*
	 * @ResponseBody
	 * 
	 * @GetMapping(value = {"/devicestring"}) public String getDeviceString(Model
	 * model) throws Exception { return "Device ID goes here"; }
	 */
	
	/*
	 * @GetMapping("/device") public String getDeviceTable(Model model,
	 * HttpServletRequest request, HttpSession session) { return "deviceTable"; }
	 */
	
	@GetMapping("/device")
	public String getDeviceTableRead(Model model, HttpServletRequest request, HttpSession session) {
		model.addAttribute("title", "Device Inventory");
		List<Device> deviceList = deviceService.getDevice();
		model.addAttribute("deviceList", deviceList);
		return "deviceTableR";
	}
	
	@GetMapping("/addEditDevice")
	public String getAddDevice(Model model, String id) {
		if (id == null || id.length() == 0) {
			
			// Adding Device (Device)
			Device device = new Device();
			model.addAttribute("command", device);
			model.addAttribute("title", "Add Device");
			
		}else {
			// Editing Device (Device)
			//model.addAttribute("command", device);
			//model.addAttribute("Title", "Edit Device");
		}
		
		return "addEditDevice";
	}

	@GetMapping("/delete")
	public String getDeleteDevice(Model model, HttpServletRequest request, HttpSession session, String id) {
		deviceService.removeDevice(model, Integer.parseInt(id));
		return "redirect:/device";
	}
	
	@PostMapping("/addEditDevice")
	public String postAddEditDevice(Model model, Device device) {	
		deviceService.saveDevice(device);
		return "redirect:/device";
	}

	
}
