package edu.missouristate.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import edu.missouristate.model.Device;

@Repository
public class DeviceRepository {

	@Autowired
	JdbcTemplate template;

	public DeviceRepository() {
	}

	public List<Device> getDevices() {
		String sql = "SELECT * " +
					 "   FROM devices ";

		//Object[] args = {};
		//List<Map<String, Object>> resultSet = template.queryForList(sql, args);
		List<Map<String, Object>> resultSet = template.queryForList(sql);
		List<Device> deviceList = new ArrayList<Device>();

		for (Map<String, Object> map : resultSet) {
			Device device = new Device();

			for (Map.Entry<String, Object> entry : map.entrySet()) {
				String key = entry.getKey();
				String data = ((entry.getValue() == null) ? null : entry.getValue().toString());

				switch(key) {
				case "BARCODE":
					device.setBarcode(data);
					break;
				case "NAME":
					device.setName(data);
					break;
				case "CATEGORY":
					device.setCategory(data);
					break;
				case "CONDITION":
					device.setCondition(data);
					break;
				case "PRICE":
					device.setPrice(Integer.valueOf(data));
					break;
				case "ID":
					device.setId(Integer.valueOf(data));
					break;
				}
			}

			if (device.getId() != null) {
				deviceList.add(device);
			}
		}

		return deviceList;

	}

	public void printList(List<?> list) {
		for (Object result : list) {
			System.out.println(result);
		}
	}
	
	public void insertDevice(Device device) {
		String SQL = "INSERT INTO devices (barcode, name, category, condition, price) " +
					  "Values (?,?,?,?,?)";
		Object[] args = { device.getBarcode(), device.getName(), device.getCategory(), device.getCondition(), device.getPrice()};
		template.update(SQL, args);
		
	}

	public void removeDevice(Integer ID) {
		String SQL = "DELETE FROM devices where id = ?";
		Object[] args = {ID};
		template.update(SQL, args);
	};
	
}