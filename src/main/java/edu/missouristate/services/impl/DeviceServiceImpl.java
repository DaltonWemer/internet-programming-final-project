package edu.missouristate.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import edu.missouristate.dao.DeviceRepository;
import edu.missouristate.model.Device;
import edu.missouristate.services.DeviceService;

@Service("deviceService")
public class DeviceServiceImpl implements DeviceService {
	
	@Autowired
	DeviceRepository deviceRepository;
	
	@Override
	public List<Device> getDevice() {
		return deviceRepository.getDevices();
	}
	
	@Override
	public void saveDevice(Device device) {
		
		if (device != null && device.getId() != null) {
			update(device);
		} else {
			insertDevice(device);
		}
	}
	
	public void insertDevice(Device device){
		deviceRepository.insertDevice(device);
	};
	
	public void update(Device device) {
		
	}

	@Override
	public void removeDevice(Model model, Integer ID) {
		deviceRepository.removeDevice(ID);
	}
	
}