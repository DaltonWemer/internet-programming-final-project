package edu.missouristate.services;

import java.util.List;

import org.springframework.ui.Model;

import edu.missouristate.model.Device;

public interface DeviceService {
	public List<Device> getDevice();
	public void saveDevice(Device device);
	public void removeDevice(Model model, Integer ID);
}

