package edu.missouristate.model;

public class Device {

	private Integer id; // id INTEGER GENERATED ALWAYS AS IDENTITY,
	private String barcode; // bar_code VARCHAR(32) NOT NULL,
	private String name; // name VARCHAR(32) NOT NULL,
	private String category; // category VARCHAR(32),
	private String condition; // condition VARCHAR(32) NOT NULL,
	private Integer price; // price INTEGER NOT NULL,

	public Device() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBarcode() {
		return barcode;
	}

	@Override
	public String toString() {
		return "Device [id=" + id + ", name=" + name + ", category=" + category
				+ ", condition=" + condition + ", price=" + price + "]";
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}
}