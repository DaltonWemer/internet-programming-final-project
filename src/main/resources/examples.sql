CREATE TABLE devices (
    id INTEGER GENERATED ALWAYS AS IDENTITY,
    barcode VARCHAR(32) NOT NULL,
    name VARCHAR(32) NOT NULL,
    category VARCHAR(32),
    condition VARCHAR(32) NOT NULL,
    price INTEGER NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO devices (barcode, name, category, condition, price)
VALUES
 ('HJK432JKJH', 'Macbook Pro', 'Laptop', 'New', 1799 ),
 ('2H3J4GDHUJ', 'Macbook Pro', 'Laptop', 'Open Box', 1499 ),
 ('BXH2J3HDJF', 'iPhone 11 Pro Max', 'Phone', 'New', 1199 ),
 ('9FJEK37583', 'Nvidia RTX Titan', 'Graphics Card', 'New', 1399 ),
 ('2H3JXNCB3H', 'LG B9 Oled', 'TV', 'New', 1999 );