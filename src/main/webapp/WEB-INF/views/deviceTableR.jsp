<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<%@ include file="/WEB-INF/layouts/head.jsp"%>

<style type="text/css">
	.background {
		background-color: #111;
	}
	.logo{
		height: 50px;
		width: auto;
		align-self: flex-end;
	}
	
	.rounded {
		border-radius: 20px;
	}	
	.title {
		color: #e5e5e5;
	}
	.yellow {
		background-color: #FDEE16;
	}
	.full-width {
		flex:1
	}
	.red {
		color: rbg(255,0,0);
	}
</style>

<body id="demo-body" class="background">
	<div id="demo-main-div" class="container-fluid ">
		<div class="row">
			<nav class="navbar navbar-dark bg-dark full-width d-flex">
				 <img class='logo' src='<c:url value="/resources/images/decent_deal.png"></c:url>' />    
			</nav>
			<div class="col-sm-12">
				<h1 class = 'title'>Available Items</h1>
				<div class="row mb-2">
				</div>
				<c:if test="${empty deviceList}">
					There are no items in the inventory database. 
				</c:if>

				<table class="table table-bordered table-dark table-hover rounded">
					<tr>
						<th>ID</th>
						<th>Barcode</th>
						<th>Name</th>
						<th>Category</th>
						<th>Condition</th>
						<th>Price</th>
						<th>Actions</th>
					</tr>

					<c:if test="${not empty deviceList}">
						<c:forEach var="device" items="${deviceList}">
							<tr>
								<td>${device.id}</td>
								<td>${device.barcode}</td>
								<td>${device.name}</td>
								<td>${device.category}</td>
								<td>${device.condition}</td>
								<td>$ ${device.price}</td>
								<td><a href="/delete?id=${device.id}"><i class="fas fa-trash-alt red"></i></a></td>
							</tr>
						</c:forEach>
					</c:if>
				</table>
				<a href="<%=request.getContextPath()%> /addEditDevice" class="btn btn-warning btn-block yellow "> 
					Add Device 
				</a>
			</div>
		</div>
	</div>
</body>
</html>