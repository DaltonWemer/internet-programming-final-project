<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<%@ include file="/WEB-INF/layouts/head.jsp"%>
<style type="text/css">

	.background {
		background-color: #111;
	}
	.title {
		color: #e5e5e5;
	}
	form {
    	display: inline-block;
	}
	body {
    	text-align: center;
	}
	.light-background {
	 	background-color: rgb(52,58,64);
	 	margin-top: 20%;
	 	border-radius: 5px;
	 	padding-left: 50px;
	 	padding-right: 50px;
	 	padding-top: 20px;
	 	padding-bottom: 20px;
	 	
	}
	.full-width {
		flex:1
	}
	.logo{
		height: 50px;
		width: auto;
		align-self: flex-end;
	}
	.yellow {
		background-color: #FDEE16;
		color: #111
	}
	
</style>
<body id="demo-body" class = "background">
	<div id="demo-main-div" class="container-fluid">
		<div class="row">
			<nav class="navbar navbar-dark bg-dark full-width d-flex">
				 <img class='logo' src='<c:url value="/resources/images/decent_deal.png"></c:url>' />    
			</nav>
			<div class="col-md-12">
				<form:form 
					method="post" 
					id="deviceForm" 
					modelAttribute="command"
					action="${pageContext.request.contextPath}/addEditDevice"
				>
					<div class="form-group col-md-12 light-background">	
						<h1 class="title">${title}</h1>
						
						<label class="title" for="name">Device Name</label>
						<form:input class="form-control mb-2" placeholder="Name" path="name" type="text"  disabled="disabled"/>
						
						<label class="title" "for="category">Device Category</label>
						<form:input class="form-control mb-2" placeholder="Category" path="category" type="text"  disabled="disabled"/>
						
						<label class="title" for="condition">Device Condition</label>
						<form:input class="form-control mb-2" placeholder="Condition" path="condition" type="text" disabled="disabled"/>
						
						<label class="title" for="price">Price</label>
						<form:input class="form-control mb-2" placeholder="Price" path="price" type="number" disabled="disabled"/>
						
						<label class="title" for="barcode">Barcode</label>
						<form:input class="form-control mb-2" placeholder="Barcode" path="barcode" type="text"  disabled="disabled"/>
						
						<button type="submit" class="btn btn-success yellow">Submit</button>
					</div>
				</form:form>	
			</div>
		</div>
	</div>
</body>
</html>